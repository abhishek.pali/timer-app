import Header from '../src/components/header';
import Timer from '../src/components/timer';

import React, { Component } from 'react';

class App extends Component {
    state = {}
    render() { 
        return (
            <div>
                <Header />
                <Timer />
            </div>
        );
    }
}
 
export default App;