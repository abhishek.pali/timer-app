import React, { Component } from 'react';
import "../../src/App.css";

class Timer extends Component {
    state = {
        count: 0,
        string: "",
        defaultValue: 0,
        hours: "00",
        minutes: "00",
        seconds: "00",
        showStart: true,
        showPause: false,
        showCountdownSpan: false,
        showInputBoxSpan: true,
        showCountdown: true,
        countdownColor: true
    }

    handleNumbers = (e) => {
        
        var code = (e.which) ? e.which : e.keyCode;
        if (this.state.count < 5 && ((code >= 96 && code <= 105) || (code >= 48 && code <= 57)))
            {
                if (this.state.count === 0 && (code !== 96 && code !== 48))
                    {
                        this.setState({
                            count: this.state.count + 1,
                            string: e.key,
                            defaultValue: e.key
                        });
                    }        
                if ((this.state.count >= 1 && this.state.count < 4) && 
                ((code >= 96 && code <= 105) || (code >= 48 && code <= 57)))
                    {   
                        this.setState({
                            count: this.state.count + 1,
                            string: this.state.string + e.key,
                            defaultValue: this.state.defaultValue + e.key
                        });
                    }
                if (this.state.count === 4 && ((code >= 96 && code <= 105) || (code >= 48 && code <= 57)))
                    {
                        this.setState({
                            count: this.state.count + 1,
                            string: this.state.string + e.key,
                            defaultValue: this.state.defaultValue + e.key
                        });
                    }
            }

        if (code === 8 && this.state.count >= 1)
            {   
               
                if (this.state.count > 1)
                    {
                        this.setState({
                            count: this.state.count - 1,
                            string: this.state.string.slice(0, -1),
                            defaultValue: this.state.string.slice(0, -1)
                        });
                    }
                if (this.state.count === 1)
                    {   
                        this.setState({
                            count: 0,
                            string: "",
                            defaultValue: 0
                        });
                    }         
            }
    }

    handleChange = (event) => {
        
        if (event.target.value < 60 || event.target.value === "")
            {   
                if (event.target.value.length === 2)
                    {
                        this.setState({
                            seconds: event.target.value,
                            minutes: "00",
                            hours: "00"
                        });
                    }
                else if (event.target.value.length === 1)
                    {
                        this.setState({
                            seconds: "0" + event.target.value,
                            minutes: "00",
                            hours: "00"
                        });
                    }
                else
                    {
                        this.setState({
                            seconds: "00",
                            minutes: "00",
                            hours: "00"
                        });
                    }
                
            }
        else if (event.target.value >= 60 && event.target.value < 3600)
            {
                let timerValue = event.target.value;
                let minutesValue = Math.floor(timerValue / 60);
                let secondsValue = timerValue % 60;
                if (minutesValue < 10 && secondsValue < 10)
                    {
                        this.setState({
                            minutes: "0" + minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: "00"
                        });
                    }
                else if (minutesValue < 10 && secondsValue > 10)
                    {
                        this.setState({
                            minutes: "0" + minutesValue.toString(),
                            seconds: secondsValue.toString(),
                            hours: "00"
                        });
                    }
                else if (minutesValue > 10 && secondsValue < 10)
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: "00"
                        });
                    }
                else if (minutesValue === 10 && secondsValue < 10)
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: "00"
                        });
                    }
                else
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: secondsValue.toString(),
                            hours: "00"
                        });
                    }
            }
        else
            {
                let timerValue = event.target.value;
                let hoursValue = Math.floor(timerValue / 3600);
                let minutesValue = Math.floor((timerValue % 3600) / 60);
                let secondsValue = (timerValue % 3600) % 60;
                if (hoursValue < 10 && (minutesValue < 10 && secondsValue < 10))
                    {
                        this.setState({
                            minutes: "0" + minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: "0" + hoursValue.toString()
                        });
                    }
                else if (hoursValue < 10 && (minutesValue < 10 && secondsValue >= 10))
                    {
                        this.setState({
                            minutes: "0" + minutesValue.toString(),
                            seconds: secondsValue.toString(),
                            hours: "0" + hoursValue.toString()
                        });
                    }
                else if (hoursValue < 10 && (minutesValue >= 10 && secondsValue < 10))
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: "0" + hoursValue.toString()
                        });
                    }
                else if (hoursValue < 10 && (minutesValue >= 10 && secondsValue >= 10))
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: secondsValue.toString(),
                            hours: "0" + hoursValue.toString()
                        });
                    }
                else if (hoursValue >= 10 && (minutesValue < 10 && secondsValue < 10))
                    {
                        this.setState({
                            minutes: "0" + minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: hoursValue.toString()
                        });
                    }
                else if (hoursValue >= 10 && (minutesValue < 10 && secondsValue >= 10))
                    {
                        this.setState({
                            minutes: "0" + minutesValue.toString(),
                            seconds: secondsValue.toString(),
                            hours: hoursValue.toString()
                        });
                    }
                else if (hoursValue >= 10 && (minutesValue >= 10 && secondsValue < 10))
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: "0" + secondsValue.toString(),
                            hours: hoursValue.toString()
                        });
                    }
                else 
                    {
                        this.setState({
                            minutes: minutesValue.toString(),
                            seconds: secondsValue.toString(),
                            hours: hoursValue.toString()
                        });
                    }
            }
    }

    handleStart = () => {
        if (this.state.defaultValue !== 0)
            {
                this.setState({
                    showStart: false,
                    showPause: true,
                    showCountdown: false,
                    showCountdownSpan: true,
                    showInputBoxSpan: false
                });
                this.startCountDown();
            }   
    }

    startCountDown = () => {
       this.interval = setInterval(this.tick, 1000);
    }
 
    tick = () => {
        if ((this.state.hours > 0 && this.state.hours > 10) && 
            (this.state.minutes === "00" && this.state.seconds === "00"))
            {
                this.setState({
                    hours: (Number(this.state.hours) - 1).toString(),
                    minutes: "60",
                    seconds: "60"
                });
            }
        if ((this.state.hours > 0 && this.state.hours <= 10) && 
            (this.state.minutes === "00" && this.state.seconds === "00"))
            {
                this.setState({
                    hours: "0" + (Number(this.state.hours) - 1).toString(),
                    minutes: "60",
                    seconds: "60"
                });
            }
        if ((this.state.minutes > 0 && this.state.minutes > 10) && this.state.seconds === "00")
            {
                this.setState({
                    minutes: (Number(this.state.minutes) - 1).toString(),
                    seconds: "60"
                });
            }
        if ((this.state.minutes > 0 && this.state.minutes <= 10) && this.state.seconds === "00")
            {
                this.setState({
                    minutes: "0" + (Number(this.state.minutes) - 1).toString(),
                    seconds: "60"
                });
            }
        if (this.state.seconds > 10)
            {
                this.setState({
                    seconds: (Number(this.state.seconds) - 1).toString()
                });
            }

        if (this.state.seconds <= 10 && this.state.seconds > 0)
            {
                this.setState({
                    seconds: "0" + ((Number(this.state.seconds) - 1).toString())
                });
            }

        if ((this.state.seconds === "00" && this.state.minutes === "00") && this.state.hours === "00")
            {
                clearInterval(this.interval);
                this.setState({
                    seconds: "00",
                    minutes: "00",
                    hours: "00",
                    defaultValue: 0,
                    count: 0,
                    string: "",
                    showPause: false,
                    showStart: true,
                    showCountdown: true,
                    showCountdownSpan: false,
                    showInputBoxSpan: true
                });
            }

        if ((this.state.minutes === "00" && this.state.hours === "00") && this.state.seconds <= 10)
            {
                this.setState({
                    countdownColor: false
                });
            }
        // console.log(this.state.seconds);   
    }

    handlePause = () => {
        console.log("pause clicked");
        var time = (Number(this.state.hours) * 3600) + (Number(this.state.minutes) * 60)
                    + (Number(this.state.seconds));

        this.setState({
            showPause: false,
            showStart: true,
            showCountdownSpan: false,
            showCountdown: true,
            showInputBoxSpan: true,
            defaultValue: time
        });

        if ((this.state.minutes === "00" && this.state.hours === "00") && this.state.seconds <= 10)
            {
                this.setState({
                    countdownColor: false
                });
            }

        clearInterval(this.interval);
    }

    handleReset = () => {

        this.setState({
            seconds: "00",
            minutes: "00",
            hours: "00",
            defaultValue: 0,
            count: 0,
            string: "",
            showPause: false,
            showStart: true,
            showCountdown: true,
            showCountdownSpan: false,
            showInputBoxSpan: true,
            countdownColor: true
        });
    }

    render() { 
        return (
            <div className="timer-box position-relative d-flex justify-content-center">
                <svg width="300" height="300" xmlns="http://www.w3.org/2000/svg" stroke="grey" strokeWidth="10" fill="white">
                    <circle cx="150" cy="150" r="130" />
                </svg>
                <div className={"countdown position-absolute align-items-center justify-content-center text-info " + 
                                    (this.state.showCountdown ? "d-flex" : "d-none")}>
                    {this.state.hours}:{this.state.minutes}:{this.state.seconds}:00
                </div>
                <span className={"input-box-span position-absolute align-items-center justify-content-center " + 
                                    (this.state.showInputBoxSpan ? "d-flex" : "d-none")}>
                    <input type="text" className="input-box text-info" value={this.state.defaultValue} maxLength="5" onKeyUp={this.handleNumbers}
                            onChange={this.handleChange}
                            />
                </span>
                <span className={"countdown-span position-absolute align-items-center justify-content-center " + 
                                    (this.state.showCountdownSpan ? "d-flex " : "d-none ") + 
                                    (this.state.countdownColor ? "text-info " : "text-danger ")}>
                    {this.state.hours}:{this.state.minutes}:{this.state.seconds}:00
                </span>
                <span className="button-span position-absolute d-flex align-items-center justify-content-center">
                    <button className={"start-button " + (this.state.showStart ? "" : "d-none")} onClick={this.handleStart}>Start</button>
                    <button className={"pause-button " + (this.state.showPause ? "" : "d-none")} onClick={this.handlePause}>Pause</button>
                    <button className="reset-button" onClick={this.handleReset}>Reset</button>
                </span>
            </div>
        );
    }
}
 
export default Timer;
