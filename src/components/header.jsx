import React, { Component } from 'react';
import timer from '../images/timer.png';
import "../../src/App.css";

class Header extends Component {
    render() { 
        return (
            <nav className="navbar navbar-dark bg-dark d-flex justify-content-center">      
                <img src={timer} alt="timer"/>
                <h2 className="text-light my-auto mx-1">React Timer</h2>    
            </nav>
        );
    }
}
 
export default Header;
